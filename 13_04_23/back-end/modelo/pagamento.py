from geral.config import *

class Pagamento(db.Model):
    # atributos da pessoa
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.Text)
    formapg = db.Column(db.Text)
    valor = db.Column(db.Float)
    # método para expressar a pessoa em forma de texto
    def __str__(self):
        return self.nome + "[id="+str(self.id)+ "], " +\
            self.formapg+ ", " + str(self.valor)
    # expressao da classe no formato json
    def json(self):
        return {
            "id": self.id,
            "nome": self.nome,
            "pagamento": self.formapg,
            "valor": self.valor
        }